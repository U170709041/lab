
public class MyDateTime {

	MyDate date;
	MyTime time;
	
	
	
	
	public MyDateTime(MyDate date, MyTime time) {
		this.date = date;
		this.time = time;
	}

	public String toString() {
		return date.toString() + " " + time.toString();
	}
	
	
	public void incrementDay() {
		date.incrementDay();
		
	}

	public void incrementHour() {
		date.incrementDay(time.incrementHour());
		
	}

	public void incrementHour(int i) {
		date.incrementDay(time.incrementHour(i));
		
	}

	public void decrementHour(int i) {
		date.decrementDay(time.decrementHour(i));
	}

	public void incrementMinute(int i) {
		date.incrementDay(time.incrementMinute(i));
	}

	public void decrementMinute(int i) {
		date.decrementDay(time.decrementMinute(i));
		
		
	}

	public void incrementYear(int i) {
		
		
	}

	public void decrementDay() {
			
	}

	public void decrementYear() {
		
		
	}

	public void decrementMonth() {
		
		
	}

	public void incrementDay(int i) {
		
		
	}

	public void decrementMonth(int i) {
		
	}

	public void decrementDay(int i) {
		
		
	}

	public void incrementMonth() {
	
		
	}

	public void incrementMonth(int i) {
		
		
	}

	public void decrementYear(int i) {
		
		
	}

	public void incrementYear() {
		
	}

	public boolean isBefore(MyDateTime anotherDateTime) {
		
		return false;
	}

	public boolean isAfter(MyDateTime anotherDateTime) {
	
		return false;
	}

	public String dayTimeDifference(MyDateTime anotherDateTime) {

		return null;
	}

}
